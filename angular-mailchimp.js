/**
 * angular-mailchimp
 * http://github.com/keithio/angular-mailchimp
 * License: MIT
 */

'use strict';

angular.module('mailchimp', ['ng', 'ngResource', 'ngSanitize', 'pascalprecht.translate', 'mailchimp', 'ipCookie'])

        /**
         * Form controller for a new Mailchimp subscription.
         */
    .controller('MailchimpSubscriptionCtrl', ['$log', '$resource', '$scope', '$translate', '$location', 'ipCookie',
					      function($log, $resource, $scope, $translate, $location, ipCookie) {

						  $scope.submitSubscription = function() {
						      return false;
						  }

                // Handle clicks on the form submission.
						  $scope.addSubscription = function(mailchimp) {
						      var actions,
						      MailChimpSubscription,
						      params,
						      url;
						      if ($scope.clicked || $scope.MailchimpSubscriptionForm.$invalid) {
							  return;
						      }
						      $scope.clicked = true;
                    // Create a resource for interacting with the MailChimp API



                    //url =  '//' + mailchimp.dc + '.api.mailchimp.com/2.0/lists/subscribe.json';

						      url = '//' + mailchimp.username + '.' + mailchimp.dc + '.list-manage.com/subscribe/post-json';
						      params = {
							  'EMAIL': mailchimp.email,
							  'FNAME': mailchimp.fname,
							  'LNAME': mailchimp.lname,
							  'c': 'JSON_CALLBACK',
							  'u': mailchimp.u,
                        //'id': mailchimp.id
						      };
						      actions = {
							  'save': {
							      method: 'jsonp'
							  }
						      };


                    // Send subscriber data to MailChimp
						      $scope.lang = ($location.path().substr(1, 2) === 'it' || $location.path().substr(1, 2) === 'en') ? $location.path().substr(1, 2) : ipCookie('locale') ? ipCookie('locale') : 'en';
						      params['MC_LANGUAGE'] = $scope.lang;//?serve
						      switch ($scope.lang) {
						      case 'it':
							  params['id'] = mailchimp.id;
							  break;
						      default:
							  params['id'] = mailchimp.englishid;
						      }
						      MailChimpSubscription = $resource(url, params, actions);
                    /**
                     * TODO: url da modificare in dipendenza dalla lingua (per smistare gli utenti sulla NL opportuna)
                     */

						      MailChimpSubscription.save(
                            // Successfully sent data to MailChimp.
							  function(response) {
                                        // Define message containers.
							      mailchimp.errorMessage = '';
							      mailchimp.successMessage = '';

                                        // Store the result from MailChimp
							      mailchimp.result = response.result;

                                        // Mailchimp returned an error.
							      if (response.result === 'error') {
								  if (response.msg) {
                                                // Remove error numbers, if any.
                                                /*var errorMessageParts = response.msg.split(' - ');
                                                if (errorMessageParts.length > 1) {
                                                    errorMessageParts.shift();
                                                    }*/
								      if (response.msg.indexOf("already subscribed") >= 0 || response.msg.indexOf("già presente") >= 0) {//errorMessageParts.indexOf("already subscribed") >= 0) {
									  $translate(['NEWSLETTER.MESSAGE.ERROR.SUBSCRIBED']).then(function(translations) {
                                                        //messageCenterService.add('alert', translations['NEWSLETTER.MESSAGE.ERROR.SUBSCRIBED'], {status: messageCenterService.status.permanent});
									      mailchimp.errorMessage = translations['NEWSLETTER.MESSAGE.ERROR.SUBSCRIBED'];
									  });

								      } else {
									  $translate(['NEWSLETTER.MESSAGE.ERROR.GENERIC']).then(function(translations) {
                                                        //messageCenterService.add('alert', translations['NEWSLETTER.MESSAGE.ERROR.GENERIC'], {status: messageCenterService.status.permanent});
									      mailchimp.errorMessage = translations['NEWSLETTER.MESSAGE.ERROR.GENERIC'];
									  });
								      }
                                                //mailchimp.errorMessage = errorMessageParts.join(' ');
								  } else {
								      $translate(['NEWSLETTER.MESSAGE.ERROR.GENERIC']).then(function(translations) {
                                                    //messageCenterService.add('alert', translations['NEWSLETTER.MESSAGE.ERROR.GENERIC'], {status: messageCenterService.status.permanent});
									  mailchimp.errorMessage = translations['NEWSLETTER.MESSAGE.ERROR.GENERIC'];
								      });

                                                //mailchimp.errorMessage = 'Sorry! An unknown error occured.';


								  }
							      }
                                        // MailChimp returns a success.
							      else if (response.result === 'success') {
                                            //mailchimp.successMessage = response.msg;
								  $translate(['NEWSLETTER.MESSAGE.SUCCESS']).then(function(translations) {
                                                //messageCenterService.add('success', translations['NEWSLETTER.MESSAGE.SUCCESS'], {status: messageCenterService.status.permanent});
								      mailchimp.successMessage = translations['NEWSLETTER.MESSAGE.SUCCESS'];
								      delete mailchimp.email;
								  });
							      }
							      $scope.clicked = false;
							  },
                                    // Error sending data to MailChimp
							  function(error) {
							      $log.error('MailChimp Error: %o', error);
							      $scope.clicked = false;
							  }
						      );



						      return false;
						  };
					      }]);
